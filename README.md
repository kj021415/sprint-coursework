      Module Code:  CS1PC20
                      Assignment report Title: Spring Coursework    
     Student Number : 31021415
     Date (when the work completed): 27th 
  Actual hrs spent for the assignment: 20
Assignment evaluation (3 key points): 



LAB PRACTICE 1:
#include <iostream>
using namespace std;
int main()
{
	// Declare and initialize a pointer.
	unsigned short int* pPointer = 0;
	// Declare an integer variable and initialize it with 35698
	unsigned short int twoInt = 35698;
	// Declare an integer variable and initialize it with 77
	unsigned short int oneInt = 77;
	// Use address-of operator & to assign a memory address
	pPointer = &twoInt;
	// Pointer pPointer now holds a memory address of twoInt
	// Print out associated memory addresses and its values
	cout << "pPointer's memory address:\t\t" << &pPointer << endl;
	cout << "Integer's oneInt memory address:\t" << &oneInt <<
		"\tInteger value:\t" << oneInt << endl;
	Page 4 of 16
		cout << "Integer's twoInt memory address:\t" << &twoInt <<
		"\tInteger value:\t" << twoInt << endl;
	cout << "pPointer is pointing to memory address:\t" << pPointer <<
		"\tInteger value:\t" << *pPointer << endl;
	return 0;

Corrected Code:
#include <iostream>
using namespace std;

int main() {

    unsigned short int* pPointer = nullptr;
    unsigned short int twoInt = 35698;
    unsigned short int oneInt = 77;
    pPointer = &twoInt;
    cout << "pPointer's memory address:\t\t" << &pPointer << endl;
    cout << "Integer's oneInt memory address:\t" << &oneInt << "\tInteger value:\t" << oneInt << endl;
    cout << "Integer's twoInt memory address:\t" << &twoInt << "\tInteger value:\t" << twoInt << endl;
    cout << "pPointer is pointing to memory address:\t" << pPointer << "\tInteger value:\t" << *pPointer << endl;

 
    *pPointer = 100;
    cout << "Manipulating Data with Pointers: \n";
    cout << "Integer's twoInt memory address:\t" << &twoInt << "\tInteger value:\t" << twoInt << endl;

    return 0;
}
LAB PRACTICE 2:
#include <iostream>
int main()
{
	using namespace std;
	// Declare an integer variable and initialize it with 99
	unsigned short int myInt = 99;
	// Declare and initialize a pointer
	unsigned short int* pMark = 0;
	// Print out a value of myInt
	cout << myInt << endl;
	// Use address-of operator & to assign memory address of myInt
	pMark = &myInt;
	// Dereference a pMark pointer with dereference operator *
	*pMark = 11;
	// show indirectly a value of pMark and directly the value of myInt
	cout << "*pMark:\t" << *pMark << "\nmyInt:\t" << myInt << endl;
	return 0;
}
Corrected Code:
#include <iostream>
using namespace std;

int main() {
    unsigned short int myInt = 99;
    unsigned short int* pMark = nullptr;
    cout << myInt << endl;
    pMark = &myInt;
    *pMark = 11;
    cout << "*pMark:\t" << *pMark << "\nmyInt:\t" << myInt << endl;
    return 0;
}

LAB PRACTICE 3:
#include <iostream>
int main()
{
	int x{ 5 }; // this is equivalent to int x = 5;
	std::cout << x << '\n'; // print the value of variable x
	std::cout << &x << '\n'; // print the memory address of variable x
	// print the value at the memory address of variable x
	// (parentheses not required, but make it easier to read)
	std::cout << *(&x) << '\n';
	return 0;
}
Corrected code:
#include <iostream>

int main() {
    int x{ 5 }; 
    std::cout << x << '\n';
    std::cout << &x << '\n';
    std::cout << *(&x) << '\n';

    
    char name[] = { 'A', 'S', 'A', 'D', '\0' };
    char* ptr = name;
    while (*ptr != '\0') {
        std::cout << *ptr;
        ++ptr;
    }
    std::cout << '\n';

    return 0;
}


LAB PRACTICE 4:
#include <iostream>
int main()
{
	char name[] = "Klinsman";
	int i = 0;
	while (i <= 7)
	{
		std::cout << name[i];
		i++;
	}
	return 0;
}
Corrected code:
#include <iostream>

int main() {
    char name[] = "Klinsman";
    int length = sizeof(name) / sizeof(name[0]); 
    for (int i = 0; i < length; i++) { 
        std::cout << name[i];
    }
    std::cout << '\n';
    return 0;
}


LAB PRACTICE 5:
#include <iostream>
int main()
{
	char name[] = "Klinsman";
	int i = 0;
	while (name[i] != '\0')
	{
		std::cout << name[i];
		i++;
	}
	return 0;
}
Corrected code:
#include <iostream>

int main() {
    char name[] = "Klinsman";
    int i = 0;
    while (name[i] != '\0') {
        std::cout << name[i];
        i++;
    }
    std::cout << '\n';
    return 0;
}
LAB PRACTICE 6:
#include <iostream>
using namespace std;
int main()
{
	char name[] = "Klinsman";
	char* ptr;
	ptr = name; /* store base address of string */
	while (*(ptr) != '\0')
	{
		cout << *ptr;
		ptr++; //jumps to the next address
	}
	return 0;
}

Corrected code:

#include <iostream>
using namespace std;

int main() {
    char name[] = "Klinsman";
    char* ptr;
    ptr = name; 
    while (*(ptr) != '\0') {
        cout << *ptr;
        ptr++; 
    }
    cout << '\n';
    return 0;
}

LAB PRACTICE 7:
#include <iostream>
int main()
{
	int x{ 5 }; // int x = 5;
	int& ref{ x }; // int& ref = x; get a reference to x
	int* ptr{ &x }; // int* ptr = &x ;get a pointer to x
	std::cout << x;
	std::cout << ref; // use the reference to print x's value (5)
	std::cout << *ptr << '\n'; // use the pointer to print x's value (5)
	ref = 6; // use the reference to change the value of x
	std::cout << x;
	std::cout << ref; // use the reference to print x's value (6)
	std::cout << *ptr << '\n'; // use the pointer to print x's value (6)
	*ptr = 7; // use the pointer to change the value of x
	std::cout << x;
	std::cout << ref; // use the reference to print x's value (7)
	std::cout << *ptr << '\n'; // use the pointer to print x's value (7)
	return 0;
}

LAB PRACTICE 8:
#include <iostream>
int main() // assume a 32-bit application
{
	char* chPtr{}; // char* chPtr = 0; chars are 1 byte
	int* iPtr{}; // ints are usually 4 or 8 bytes
	long double* ldPtr{}; // long doubles are usually 8 or 12 bytes
	std::cout << sizeof(chPtr) << '\n'; // prints 4 or 8
	std::cout << sizeof(iPtr) << '\n'; // prints 4 or 8
	std::cout << sizeof(ldPtr) << '\n'; // prints 4 or 8
	return 0;
}

LAB PRACTICE 9:
#include <iostream>
int main()
{
	int x{ 5 };
	int* ptr{ &x };
	std::cout << *ptr << '\n'; // valid
	{
		int y{ 6 };
		ptr = &y;
		std::cout << *ptr << '\n'; // valid
		Page 8 of 16
	} // y goes out of scope, and ptr is now dangling
	// undefined behavior from dereferencing a dangling pointer
	std::cout << *ptr << '\n';
	return 0;
}

LAB PRACTICE 10:
#include <iostream>
using namespace std;
int& bar()
{
	int n = 10;
	return n;
}
int main() {
	int& i = bar();
	i = 5;
	cout << i << endl;
	return 0;

Corrected code:
#include <iostream>

using namespace std;

int& bar() {
    static int n = 10;
    return n;
}

int main() {
    int& i = bar();
    i = 5;
    cout << i << endl; 
    return 0;
}


}
LAB PRACTICE 11:
#include <iostream>
using namespace std;
int& bar()
{
	static int n = 10;
	return n;
}
int main() {
	int& i = bar();
	cout << i << endl;
	return 0;
}

Corrected code:
#include <iostream>

using namespace std;

int& bar() {
    static int n = 10;
    return n;
}

int main() {
    int& i = bar();
    i = 5;
    cout << i << endl; 
    return 0;
}


LAB PRACTICE 12:
#include <iostream>
using namespace std;
int main()
{
	int a = 10;
	char b = 'x';
	void* p = &a; // void pointer holds address of int 'a'
	p = &b; // void pointer holds address of char 'b'
}

Corrected code:
#include <iostream>
using namespace std;
int main()
{
	int a = 10;
	char b = 'x';
	void* p = &a; 
	p = &b; 
	cout << "a value: " << *(int*)p << endl;
	cout << "b value: " << *(char*)p << endl; 

	return 0;



LAB PRACTICE 13:
#include <iostream>;
using namespace std;
int main()
{
	int a = 10;
	void* ptr = &a;
	cout << *(int*)ptr;
	return 0;
}

LAB PRACTICE 14:
#include <iostream>
using namespace std;
int main()
{
	const int numElements = 5;
	int numbers[numElements] = { 2,4,6,8,10 };
	for (int i = 0; i < numElements; i++) {
		cout << *(numbers + i) << " ";
	}
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

int main() {
	const int numElements = 5;
	int numbers[numElements] = { 2, 4, 6, 8, 10 };

	cout << "First element: " << *numbers << endl;
	cout << "Second element: " << *(numbers + 1) << endl;

	return 0;
}


LAB PRACTICE 15:
#include <iostream>
using namespace std;
int main()
{
	const char* months[] = { "Illegal month", "Jan", "Feb", "Mar" };
	cout << *months << " ";
	cout << *(months + 1) << " ";
	cout << *(months + 2) << " ";
	cout << *(months + 3) << " ";
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;
int main()
{
	const char* months[] = { "Illegal month", "Jan", "Feb", "Mar" };
	cout << months[0] << " ";
	cout << months[1] << " ";
	cout << months[2] << " ";
	cout << months[3] << " ";
	return 0;
}


LAB PRACTICE 16:
#include <iostream>
using namespace std;
int main()
{
	const int numElements = 3;
	string names[numElements] = { "Meredith", "Allison", "Mason" };
	string* names_ptr = names;
	cout << "First name: " << *names_ptr << endl;
	names_ptr++;
	cout << "Second name: " << *names_ptr << endl;
	return 0;
}
Corrected code:
#include <iostream>
using namespace std;
int main()
{
	const int numElements = 3;
	string names[numElements] = { "Meredith", "Allison", "Mason" };
	string* names_ptr = names;
	for (int i = 0; i < numElements; i++) {
		cout << *(names_ptr + i) << " ";
	}
	return 0;
}

LAB PRACTICE 17:
#include <iostream>
using namespace std;
int main()
{
	const int numElements = 3;
	string names[numElements] = { "Meredith", "Allison", "Mason" };
	string* names_ptr = names;
	for (int i = 0; i < numElements; i++) {
		cout << *names_ptr << " ";
		names_ptr++;
	}
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

int main()
{
    const int numElements = 3;
    string names[numElements] = { "Meredith", "Allison", "Mason" };
    string* names_ptr = names;

    for (int i = 0; i < numElements; i++) {
        cout << *names_ptr << " ";
        names_ptr++;
    }

    return 0;
}


LAB PRACTICE 18:
#include <iostream>
using namespace std;
int main()
{
	const int numElements = 3;
	string names[numElements] = { "Meredith", "Allison", "Mason" };
	string* names_ptr = names;
	for (int i = 0; i < numElements; i++) {
		cout << *names_ptr << " ";
		names_ptr++;
	}
	for (int i = 0; i < numElements; i++) {
		names_ptr--;
		cout << *names_ptr << " ";
	}
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

int main() {
    const int numElements = 3;
    string names[numElements] = { "Meredith", "Allison", "Mason" };
    string* names_ptr = &names[numElements - 1]; 

    for (int i = 0; i < numElements; i++) {
        cout << *names_ptr << " ";
        names_ptr--; 
    }
    return 0;
}



LAB PRACTICE 19:
#include <iostream>
using namespace std;
Page 12 of 16
int main()
{
	const int numElements = 5;
	int numbers[numElements] = { 23, 1, 45, 67, 13 };
	int* ptr = numbers;
	while (ptr < &numbers[numElements]) {
		cout << *ptr << " ";
		ptr++;
	}
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

int main()
{
	const int numElements = 5;
	int numbers[numElements] = { 23, 1, 45, 67, 13 };
	int* ptr = numbers;

	while (ptr < &numbers[numElements]) {
		cout << *ptr << " ";
		ptr++;
	}

	return 0;
}


LAB PRACTICE 20:
#include <stdio.h>
void swapx(int*, int*); // Function Prototype
int main() { // Main function
	int a = 10, b = 20;
	swapx(&a, &b); // Pass reference
	printf("a=%d b=%d\n", a, b);
	return 0;
}
void swapx(int* x, int* y) { // Function to swap two variables by references
	int t;
	t = *x;
	*x = *y;
	*y = t;
	printf("x=%d y=%d\n", *x, *y);
}

Corrected code:
#include <iostream>
using namespace std;

void swapx(int*, int*); 

int main() { 
    int a = 10, b = 20;
    swapx(&a, &b); 
    cout << "a=" << a << " b=" << b << endl;
    return 0;
}

void swapx(int* x, int* y) { 
    int t;
    t = *x;
    *x = *y;
    *y = t;
    cout << "x=" << *x << " y=" << *y << endl;
}


LAB PRACTICE 21:
#include <iostream>
void swap(int& first, int& second) { // Function to swap two variables by
	references
		int temp;
	temp = first;
	first = second;
	second = temp;
}
int main() { // Main function
	int a = 5;
	int b = 3;
	swap(a, b);
	std::cout << "a = " << a << ", b = " << b << std::endl;
	return 0;
}

Corrected code:
#include <iostream>

void swap(int& first, int& second) { 
    int temp;
    temp = first;
    first = second;
    second = temp;
}

int main() { 
    int a = 5;
    int b = 3;
    swap(a, b);
    std::cout << "a = " << a << ", b = " << b << std::endl;
    return 0;
}


LAB PRACTICE 22:
#include <iostream>
void print(const char* message) {
	std::cout << message;
}
int main()
{
	print("Hello");
	return 0;
}
LAB PRACTICE 23:
#include <iostream>
void print(const char* message) {
	std::cout << message;
}
int main()
{
	const char* pmessage;
	pmessage = "Hello";
	print(pmessage);
	return 0;
}

Corrected code:
#include <iostream>

void print(const char* message) {
	std::cout << message;
}

int main()
{
	const char* const pmessage = "Hello";
	print(pmessage);
	return 0;
}


LAB PRACTICE 24:
#include <iostream>
using namespace std;
Page 14 of 16
struct employee {
	char name[100];
	int age;
	float salary;
	char department[50];
};
int main() {
	struct employee manager, * ptr;
	printf("Enter Name, Age, Salary and Department of Employee\n");
	// Assigning data to members of structure variable
	cin >> manager.name >> manager.age >> manager.salary >> manager.department;
	/* Printing structure members using arrow operator */
	ptr = &manager;
	cout << "\nEmployee Details\n";
	cout << "Name : " << ptr->name << "\nAge : " << ptr->age << "\nSalary : "
		<< ptr->salary << "\nDepartment : " << ptr->department;
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

struct employee {
    char name[100];
    int age;
    float salary;
    char department[50];
};

int main() {
    struct employee manager, * ptr;
    printf("Enter Name, Age, Salary and Department of Employee\n");
    cin >> manager.name >> manager.age >> manager.salary >> manager.department;
    ptr = &manager;
    cout << "\nEmployee Details\n";
    cout << "Name : " << ptr->name << "\nAge : " << ptr->age << "\nSalary : "
        << ptr->salary << "\nDepartment : " << ptr->department;
    return 0;
}


LAB PRACTICE 25:
#include <iostream>
using namespace std;
int main() {
	int var;
	int* ptr;
	int** pptr;
	var = 3000;
	// take the address of var
	ptr = &var;
	// take the address of ptr using address of operator &
	pptr = &ptr;
	// take the value using pptr
	cout << "Value of var :" << var << endl;
	cout << "Value available at *ptr :" << *ptr << endl;
	cout << "Value available at **pptr :" << **pptr << endl;
	return 0;
}

Corrected code:
#include <iostream>
using namespace std;

int main() {
    int var;
    int* ptr;
    int** pptr;

    var = 3000;

    ptr = &var;

    pptr = &ptr;

    cout << "Value of var: " << var << endl;
    cout << "Value available at *ptr: " << *ptr << endl;
    cout << "Value available at **pptr: " << **pptr << endl;

    return 0;
}




Last Task

#include <iostream>

using namespace std;

const int MAX_ROWS = 5;
const int MAX_COLS = 5;

void matrixAddition(int* mat1, int* mat2, int* res, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            *(res + i * MAX_COLS + j) = *(mat1 + i * MAX_COLS + j) + *(mat2 + i * MAX_COLS + j);
        }
    }
}

int main() {
    int rows, cols;
    int matrix1[MAX_ROWS][MAX_COLS], matrix2[MAX_ROWS][MAX_COLS], result[MAX_ROWS][MAX_COLS];
    int* ptr1 = &matrix1[0][0], * ptr2 = &matrix2[0][0], * res_ptr = &result[0][0];

    cout << "Please enter the number of rows (max 5) = ";
    cin >> rows;
    cout << "Please enter the number of columns (max 5) = ";
    cin >> cols;

    cout << "1st Matrix Input:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << "matrix1[" << i << "][" << j << "]= ";
            cin >> *(ptr1 + i * MAX_COLS + j);
        }
    }

    cout << "2nd Matrix Input:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << "matrix2[" << i << "][" << j << "]= ";
            cin >> *(ptr2 + i * MAX_COLS + j);
        }
    }

    matrixAddition(ptr1, ptr2, res_ptr, rows, cols);

    cout << "The resultant Matrix is:" << endl;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            cout << *(res_ptr + i * MAX_COLS + j) << " ";
        }
        cout << endl;
    }

    return 0;
}

WEEK 3

LAB PRACTICE 1:
#include <iostream>
int main()
{
	int x = 10;
	int* ptr;
	ptr = new int;
	*ptr = x;
	std::cout << *ptr;
	delete ptr;
}
Corrected code:
#include <iostream>

int main()
{
    int x = 10;
    int* ptr;
    ptr = new int;
    *ptr = x;
    std::cout << *ptr;
    delete ptr;
    ptr = nullptr; 
    return 0;
}


LAB PRACTICE 2:
#include <iostream>
void main() {
	int SIZE;
	std::cout << "Enter array size: ";
	std::cin >> SIZE;
	int* arr;
	arr = new int[SIZE]; // allocation
	std::cout << "Memory of " << SIZE <<
		" ints (array) has been dynamically allocated";
	delete[] arr; // deallocation
}

Corrected code:
#include <iostream>
int main() {
    int SIZE;
    std::cout << "Enter array size: ";
    std::cin >> SIZE;
    int* arr;
    arr = new int[SIZE]; 
    if (arr == nullptr) {
        std::cout << "Failed to allocate memory!" << std::endl;
        return 1;
    }
    std::cout << "Memory of " << SIZE << " ints (array) has been dynamically allocated";
    delete[] arr;
    return 0;
}

LAB PRACTICE 3:
#include <iostream>
// “#include <new>” was needed in old compilers but probably not anymore
int main()
{
	int i, n;
	int* p;
	std::cout << "How many numbers would you like to type? ";
	std::cin >> i;
	p = new (std::nothrow) int[i];
	if (p == nullptr)
		std::cout << "Error: memory could not be allocated";
	std::cout << "Memory has been dynamically allocated";
	return 0;
}

Corrected code:
#include <iostream>
#include <new>

int main()
{
    int i, n;
    int* p;
    std::cout << "How many numbers would you like to type? ";
    std::cin >> i;
    p = new (std::nothrow) int[i];
    if (p == nullptr)
        std::cout << "Error: memory could not be allocated";
    else
        std::cout << "Memory has been dynamically allocated";
    delete[] p;
    return 0;
}


LAB PRACTICE 4:
#include <iostream>
using namespace std;
void main() {
	int numRows, numCols;
	cout << "Enter numRows: ";
	cin >> numRows;
	cout << "Enter numCols: ";
	cin >> numCols;
	int** arr2D;
	arr2D = new int* [numRows]; // allocation
	Page 3 of 8
		for (int i = 0; i < numRows; i++)
			arr2D[i] = new int[numCols];
	for (int i = 0; i < numRows; i++) // deallocation
		delete[] arr2D[i];
	delete[] arr2D;
}

Corrected code:
#include <iostream>
using namespace std;

int main() {
    int numRows, numCols;
    cout << "Enter numRows: ";
    cin >> numRows;
    cout << "Enter numCols: ";
    cin >> numCols;

    int** arr2D;
    arr2D = new (nothrow) int* [numRows]; 

    for (int i = 0; i < numRows; i++) {
        arr2D[i] = new (nothrow) int[numCols];
    }


    for (int i = 0; i < numRows; i++) { 
        delete[] arr2D[i];
    }
    delete[] arr2D;

    return 0;
}


TASK for week 3
#include "floatList.h"
#include <iostream>

using namespace std;

void floatList::appendNode(float num) {
    ListNode* newNode; 
    ListNode* nodePtr; 

    newNode = new ListNode;
    newNode->value = num;
    newNode->next = nullptr;

    if (!head)
        head = newNode;
    else  
    {
    
        nodePtr = head;

        while (nodePtr->next)
            nodePtr = nodePtr->next;

        nodePtr->next = newNode;
    }
}

void floatList::displayList() {
    ListNode* nodePtr; 

    nodePtr = head;

    while (nodePtr) {
        
        cout << nodePtr->value << endl;

        nodePtr = nodePtr->next;
    }
}

void floatList::deleteNode(float num) {
    ListNode* nodePtr;       
    ListNode* previousNode;  

    if (!head)
        return;

    if (head->value == num) {
        nodePtr = head->next;
        delete head;
        head = nodePtr;
    }
    else {
        nodePtr = head;

        while (nodePtr != nullptr && nodePtr->value != num) {
            previousNode = nodePtr;
            nodePtr = nodePtr->next;
        }
        if (nodePtr) {
            previousNode->next = nodePtr->next;
            delete nodePtr;
        }
    }
}

Week 4
There are two files
Flotlist.cpp file 
#include "floatList.h"
#include <iostream>

void FloatList::appendNode(float num)
{
    ListNode* newNode; 
    ListNode* nodePtr; 

    newNode = new ListNode;
    newNode->value = num;
    newNode->next = nullptr;


    if (!head)
        head = newNode;
    else 
    {
        nodePtr = head;

        while (nodePtr->next)
            nodePtr = nodePtr->next;

        nodePtr->next = newNode;
    }
}

void FloatList::displayList() {
    ListNode* nodePtr = head;
    while (nodePtr) {
        std::cout << nodePtr->value << std::endl;
        nodePtr = nodePtr->next;
    }
}

FloatList::ListNode* mergeLists(FloatList::ListNode* l1, FloatList::ListNode* l2) {
    FloatList::ListNode* result = nullptr;
    if (l1 == nullptr)
        return l2;
    if (l2 == nullptr)
        return l1;
    if (l1->value <= l2->value) {
        result = l1;
        result->next = mergeLists(l1->next, l2);
    }
    else {
        result = l2;
        result->next = mergeLists(l1, l2->next);
    }
    return result;
}

FloatList::ListNode* removeDuplicatesInMergedLists(FloatList::ListNode* l3) {
    FloatList::ListNode* current = l3;
    while (current != nullptr && current->next != nullptr) {
        if (current->value == current->next->value) {
            FloatList::ListNode* temp = current->next;
            current->next = current->next->next;
            delete temp;
        }
        else {
            current = current->next;
        }
    }
    return l3;
}


Floatlist.h file
#pragma once

class FloatList {
public:
    struct ListNode {
        float value;
        ListNode* next;
    };
    ListNode* head;
    FloatList(void) { head = nullptr; }
    ~FloatList(void) { };
    void appendNode(float);
    void displayList(void);
    static ListNode* mergeLists(ListNode*, ListNode*);
    static ListNode* removeDuplicatesInMergedLists(ListNode*);
};

Main.cpp file 
#include <iostream>
#include "floatList.h"

int main(void)
{
    FloatList list1;
    list1.appendNode(1);
    list1.appendNode(5);
    list1.appendNode(9);
    list1.appendNode(14);
    list1.appendNode(15);
    list1.appendNode(21);
    std::cout << "Printing list 1: " << std::endl;
    list1.displayList();

    FloatList list2;
    list2.appendNode(9);
    list2.appendNode(10);
    list2.appendNode(14);
    list2.appendNode(19);
    list2.appendNode(29);
    list2.appendNode(37);
    std::cout << "Printing list 2: " << std::

WEEK 5

I pushed it several times and it did not work at all. I have screenshots to prove it.

So this is the rest of my project written out as I was not able to push it for some reason.
I changed 3 things to the game
first one making the blocks green and i had to download a texture pack for it
GLuint textureID;
int width, height;
unsigned char* image = SOIL_load_image("gfx/block.png", &width, &height, 0, SOIL_LOAD_RGB);
glGenTextures(1, &textureID);
glBindTexture(GL_TEXTURE_2D, textureID);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
SOIL_free_image_data(image);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

// Load the shader program
GLuint shaderProgram = createShaderProgram("shader.vert", "shader.frag");

// Get the uniform location for the color filter
GLint colorFilterLoc = glGetUniformLocation(shaderProgram, "colorFilter");

// Set the uniform value for the color filter
glUseProgram(shaderProgram);
glUniform4f(colorFilterLoc, 0.0f, 1.0f, 0.0f, 1.0f);

// Render the texture
glUseProgram(shaderProgram);
glBindTexture(GL_TEXTURE_2D, textureID);
glBindVertexArray(VAO);
glDrawArrays(GL_TRIANGLES, 0, 6);

the second one whre i changed the background colour to blue 
void display() {
    // Set the clear color to blue
    glClearColor(0.0, 0.0, 1.0, 1.0);

    // Clear the window
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw your game objects here

    // Swap the buffers
    glutSwapBuffers();
}

int main(int argc, char** argv) {
    // Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

    // Create a window
    glutInitWindowSize(640, 480);
    glutCreateWindow("My Game");

    // Register the display function
    glutDisplayFunc(display);

    // Enter the main loop
    glutMainLoop();

    return 0;
}

I also changed the speed of which the character ran and how high he could jump.
I will be attaching a word document showing that I pushed but it never worked and I had done this several times. 

